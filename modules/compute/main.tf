// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

// A single Google Cloud Engine instance
resource "google_compute_instance" "default" {
 name         = "flask-vm-${random_id.instance_id.hex}"
 machine_type = "${var.machine_type}"
 zone         = "${var.region}-a"

    boot_disk {
        initialize_params {
        image = "debian-cloud/debian-9"
        }
    }

    ## This will upload the local ssh key to the instance.
    ##### CHANGE TO YOUR USERNAME #####
    metadata {
        sshKeys = "robban:${file("~/.ssh/id_rsa.pub")}"
        }

    // Make sure flask is installed on all new instances for later steps
    metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask;"

    network_interface {
    network = "default"

        access_config {
            // Include this section to give the VM an external ip address
        }
    }
}

### OUTPUTS ###
#Output the IP Address of the instance

output "IP_Address" {
    value = "${module.compute.ip}"
}

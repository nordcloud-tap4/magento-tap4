variable "env"{
    description = "env: dev, stg or prod"
}
variable "region" {
    description = "Where to run the environment"
    type = "map"
}

variable "machine_type" {
    description = "Deciding the size of the mashine"
    type = "map"
}

variable "project" {
    description = "The desired project ID"
    type = "map"
  
}

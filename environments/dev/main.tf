### MAIN TERRAFORM TEMPLATE ###

// Configure the Google Cloud provider
provider "google" {
 credentials = "${file("CREDENTIALS_FILE.json")}"
 project     = "${lookup(var.project, var.env)}"
 region      = "${lookup(var.region, var.env)}"
}

module "compute" {
    source = "modules/compute"
    machine_type = "${lookup(var.machine_type, var.env)}"
    region = "${lookup(var.region, var.env)}"
}

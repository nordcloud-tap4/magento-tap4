# This file breaks out information from variables.tf
# This should/can/will contain sensitive information such as API keys and such...

### Template map - Copy paste
# variable_name = {
#     dev  = ""
#     stg  = ""
#     prod = ""
# }

region = {
    dev  = "europe-north1"
    stg  = "europe-north1"
    prod = "europe-north1"
}

machine_type = {
    dev  = "f1-micro"
    stg  = "f1-micro"
    prod = "f1-micro"
}

project = {
    dev  = "${file("gcp-dev-project.txt")}"
    stg  = "THIS-PROJECT-IS-NOT-SPECIFIED"
    prod = "THIS-PROJECT-IS-NOT-SPECIFIED"
}
